Found a bug in the SL4A code.

When a user takes a Photo with a file which is just the filename (no parent path)
you get the following stack trace:

E/AndroidRuntime(22688): FATAL EXCEPTION: main
E/AndroidRuntime(22688): java.lang.NullPointerException
E/AndroidRuntime(22688): at com.googlecode.android_scripting.FileUtils.makeDirectories(FileUtils.java:111)
E/AndroidRuntime(22688): at com.googlecode.android_scripting.facade.CameraFacade$2.onPictureTaken(CameraFacade.java:151)
E/AndroidRuntime(22688): at android.hardware.Camera$EventHandler.handleMessage(Camera.java:734)
E/AndroidRuntime(22688): at android.os.Handler.dispatchMessage(Handler.java:99)
E/AndroidRuntime(22688): at android.os.Looper.loop(Looper.java:137)
E/AndroidRuntime(22688): at android.app.ActivityThread.main(ActivityThread.java:4745)
E/AndroidRuntime(22688): at java.lang.reflect.Method.invokeNative(Native Method)
E/AndroidRuntime(22688): at java.lang.reflect.Method.invoke(Method.java:511)
E/AndroidRuntime(22688): at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:786)
E/AndroidRuntime(22688): at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:553)
E/AndroidRuntime(22688): at dalvik.system.NativeStart.main(Native Method)

This is because the File argument passed to FileUtil#makeDirectories does not catch
the null parent file.