A Android Beanshell scripting project to put your fridge online using an Android
device. This project is not entirely serious in its aims, however it is intended
to demonstrate the capabilities of Android devices and the possibilities of 
what can be done using a scripting language.